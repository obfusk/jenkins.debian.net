#!/bin/bash

# Set up the build environment for a janitor worker.

set -e

DEBUG=true
. /srv/jenkins/bin/common-functions.sh
common_init "$@"

JANITOR_REPO_URL=https://salsa.debian.org/jelmer/debian-janitor

TARGET=/srv/janitor

sudo mkdir -p $TARGET/
sudo chown jenkins:jenkins $TARGET/

debcargo update

ARCH=$(dpkg-architecture -qDEB_HOST_ARCH)
DISTRO=unstable
CHROOT=$DISTRO-$ARCH-sbuild

if [ ! -d $TARGET/debian-janitor ]; then
	git clone --recurse $JANITOR_REPO_URL $TARGET/debian-janitor
else
	pushd $TARGET/debian-janitor
	git status || /bin/true
	git fetch
	git reset --hard origin/master
	git submodule update --init
	popd
fi

mkdir -p $TARGET/tmp
sudo chown jenkins:jenkins $TARGET/tmp

make -C $TARGET/debian-janitor/breezy realclean all
make -C $TARGET/debian-janitor/dulwich

cp $TARGET/debian-janitor/sbuildrc $TARGET/sbuildrc
sed -i "s/\$piuparts_opts = \\['--schroot', '%r-%a-sbuild' \\];/\$piuparts_opts = ['--schroot', '%r-%a-sbuild', '--no-eatmydata' ];/" $TARGET/sbuildrc
sed -i 's/%r/unstable/' $TARGET/sbuildrc

mkdir -p $TARGET/chroots

if [ -d $TARGET/chroots/janitor-$DISTRO-$ARCH ] ; then
	echo "updating existing chroot for $DISTRO/$ARCH."
	sudo sbuild-update -udcar $CHROOT
else
	echo "preparing chroot for $DISTRO/$ARCH."
	sudo sbuild-createchroot \
	     --extra-repository="deb-src http://deb.debian.org/debian/ $DISTRO main" \
	     --include=eatmydata \
	     $DISTRO $TARGET/chroots/janitor-$DISTRO-$ARCH http://deb.debian.org/debian
fi

# vim: set sw=0 noet :
