#!/bin/bash
# vim: set noexpandtab:

# Copyright 2021 Holger Levsen <holger@layer-acht.org>
# Copyright 2021 Roland Clobus <rclobus@rclobus.nl>
# released under the GPLv2

DEBUG=true
. /srv/jenkins/bin/common-functions.sh
common_init "$@"

# common code for tests.reproducible-builds.org
. /srv/jenkins/bin/reproducible_common.sh
set -e
set -o pipefail		# see eg http://petereisentraut.blogspot.com/2010/11/pipefail.html

output_echo() {
   echo "###########################################################################################"
   echo
   echo -e "$(date -u) - $1"
   echo
}

cleanup() {
   # Cleanup the workspace
   if [ ! -z "${BUILDDIR}" ] ; then
      sudo rm -rf --one-file-system ${BUILDDIR}
   fi
   # Cleanup the results
   if [ ! -z "${RESULTSDIR}" ] ; then
      rm -rf --one-file-system ${RESULTSDIR}
   fi
}

#
# main: follow https://wiki.debian.org/ReproducibleInstalls/LiveImages
#

# Cleanup if something goes wrong
trap cleanup INT TERM EXIT

# Validate commandline arguments
# Argument 1 = image type
case $1 in
  "smallest-build") export INSTALLER="none"; export PACKAGES=""; ;;
  "cinnamon")       export INSTALLER="live"; export PACKAGES="live-task-cinnamon"; ;;
  "gnome")          export INSTALLER="live"; export PACKAGES="live-task-gnome"; ;;
  "kde")            export INSTALLER="live"; export PACKAGES="live-task-kde"; ;;
  "lxde")           export INSTALLER="live"; export PACKAGES="live-task-lxde"; ;;
  "lxqt")           export INSTALLER="live"; export PACKAGES="live-task-lxqt"; ;;
  "mate")           export INSTALLER="live"; export PACKAGES="live-task-mate"; ;;
  "standard")       export INSTALLER="live"; export PACKAGES="live-task-standard"; ;;
  "xfce")           export INSTALLER="live"; export PACKAGES="live-task-xfce"; ;;
  *) output_echo "Bad argument 1: $1"; exit 1; ;;
esac
export CONFIGURATION="$1"

# Argument 2 = Debian version
# No further validation required, reproducible.yaml should be correct
if [ -z "$2" ] ; then
   output_echo "Bad argument 2: it is empty"
   exit 2
fi
export DEBIAN_VERSION="$2"

# randomize start time by 1-23 sec
delay_start

# Cleanup possible artifacts of a previous build (see reproducible_common.sh for the path)
rm -rf $BASE/debian_live_build/artifacts/r00t-me/${CONFIGURATION}_tmp-*

# Generate and use an isolated workspace
export PROJECTNAME="live-build"
mkdir -p /srv/workspace/live-build
export BUILDDIR=$(mktemp --tmpdir=/srv/workspace/live-build -d -t ${CONFIGURATION}.XXXXXXXX)
cd ${BUILDDIR}
export RESULTSDIR=$(mktemp --tmpdir=/srv/reproducible-results -d -t ${PROJECTNAME}-${CONFIGURATION}-XXXXXXXX) # accessible in schroots, used to compare results

# Fetch and use the latest version of live build
export LIVE_BUILD=${BUILDDIR}/latest-live-build
git clone https://salsa.debian.org/live-team/live-build.git ${LIVE_BUILD} --single-branch --no-tags
pushd ${LIVE_BUILD}
output_echo "Received git version $(git log -1 --pretty=oneline)"
popd

export LB_OUTPUT=${RESULTSDIR}/lb_output.txt
# Use 'now' as the basic timestamp
export SOURCE_DATE_EPOCH=$(date +%s)

# Configuration for the smallest live image (mini, without installer)
# - For /etc/apt/sources.list: Use the mirror from ${MIRROR}, no security, no updates
# - Version to build for: bullseye
# - No installer
# - Don't cache the downloaded content, re-download for the second build
# - Explicitly use the proxy that is set by ${http_proxy} to reduce some network traffic
lb config \
	--parent-mirror-bootstrap ${MIRROR} \
	--parent-mirror-binary ${MIRROR} \
	--security false \
	--updates false \
	--distribution ${DEBIAN_VERSION} \
	--debian-installer ${INSTALLER} \
	--cache-packages false \
	--apt-http-proxy ${http_proxy} \
	2>&1 | tee $LB_OUTPUT
RESULT=$?
if [ "$RESULT" != "0" ] ; then
   output_echo "Warning: lb config failed with $RESULT"
fi

if [ ! -z "${PACKAGES}" ]; then
   echo "${PACKAGES}" > config/package-lists/desktop.list.chroot
fi

# Add additional hooks, that work around known reproducible issues
# Note: Keep the hooks in sync with https://wiki.debian.org/ReproducibleInstalls/LiveImages
cat > config/hooks/normal/1000-reproducible-function-uuid_generate_random.hook.chroot << EOF
#!/bin/sh
set -e

# util-linux creates random UUIDs when uuid_generate_random is called
# Use LD_PRELOAD to replace uuid_generate_random with a less random version

# Don't run if gcc is not installed
if [ ! -e /usr/bin/cc ];
then
  exit 0
fi

cat > unrandomize_uuid_generate_random.c << END_OF_SOURCE
#include <stdlib.h>
#include <stdio.h>

#define SEQUENCE_FILENAME "/var/cache/unrandomize_uuid_generate_random.sequence_number"

/* https://tools.ietf.org/html/rfc4122 */
typedef unsigned char uuid_t[16];

/* Our pseudo-random version */
void uuid_generate_random(uuid_t out)
{
  /* Nil UUID */
  for (int i=0;i<16;i++) {
    out[i] = 0x00;
  }
  out[6]=0x40; /* UUID version 4 means randomly generated */
  out[8]=0x80; /* bit7=1,bit6=0 */

  /* The file doesn't need to exist yet */
  FILE *f = fopen(SEQUENCE_FILENAME, "rb");
  if (f) {
    fread(out+12, 4, 1, f);
    fclose(f);
  }
  /* Use the next number. Endianness is not important */
  (*(unsigned long*)(out+12))++;

  unsigned long long epoch;
  /* Use SOURCE_DATE_EPOCH when provided */
  char *date = getenv("SOURCE_DATE_EPOCH");
  if (date) {
    epoch = strtoll(date, NULL, 10);
  } else {
    epoch = 0ll;
  }
  out[0] = (epoch & 0xFF000000) >> 24;
  out[1] = (epoch & 0x00FF0000) >> 16;
  out[2] = (epoch & 0x0000FF00) >>  8;
  out[3] = (epoch & 0x000000FF);

  /* Write the sequence number */
  f = fopen(SEQUENCE_FILENAME, "wb");
  if (f) {
    fwrite(out+12, 4, 1, f);
    fclose(f);
  }
}
END_OF_SOURCE
/usr/bin/cc -shared -fPIC unrandomize_uuid_generate_random.c -Wall --pedantic -o /usr/lib/unrandomize_uuid_generate_random.so
rm -f unrandomize_uuid_generate_random.c
EOF
cat > config/hooks/normal/1001-reproducible-fontconfig.hook.chroot << EOF
#!/bin/sh
set -e

# fontconfig creates non-reproducible files with UUIDs
# See https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=864082
#
# Because the UUIDs should not be deleted, the proposed work-around is:
# * Use LD_PRELOAD to replace uuid_generate_random with a less random version

# Don't run if fontconfig is not installed
if [ ! -e /usr/bin/fc-cache ];
then
  exit 0
fi

# Don't run if the LD_PRELOAD module is not compiled
if [ ! -e /usr/lib/unrandomize_uuid_generate_random.so ];
then
  exit 0
fi

LD_PRELOAD=/usr/lib/unrandomize_uuid_generate_random.so /usr/bin/fc-cache --force --really-force --system-only --verbose
EOF
cat > config/hooks/normal/1002-reproducible-mdadm.hook.chroot << EOF
#!/bin/sh
set -e

# mkconf of mdadm creates a file with a timestamp
# A bug report with patch is available at https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=982607
# This script duplicates that patch

# Don't run if mdadm is not installed
if [ ! -e /usr/share/mdadm/mkconf ];
then
  exit 0
fi

# If mkconf already contains references to SOURCE_DATE_EPOCH, there is no need to patch the file
if grep -q SOURCE_DATE_EPOCH /usr/share/mdadm/mkconf;
then
  exit 0
fi
sed -i -e '/# This configuration was auto-generated on/cif [ -z \$SOURCE_DATE_EPOCH ]; then\n  echo "# This configuration was auto-generated on \$(date -R) by mkconf"\nelse\n  echo "# This configuration was auto-generated on \$(date -R --utc -d@\$SOURCE_DATE_EPOCH) by mkconf"\nfi' /usr/share/mdadm/mkconf
EOF
cat > config/hooks/normal/1003-reproducible-plymouth.hook.chroot << EOF
#!/bin/sh
set -e

# The hook of plymouth in update-initramfs calls fc-cache

# Don't run if plymouth is not installed
if [ ! -e /usr/share/initramfs-tools/hooks/plymouth ];
then
  exit 0
fi

# Don't patch if the LD_PRELOAD module is not compiled
if [ ! -e /usr/lib/unrandomize_uuid_generate_random.so ];
then
  exit 0
fi

# If the hook already contains references to LD_PRELOAD, there is no need to patch the file
if grep -q LD_PRELOAD /usr/share/initramfs-tools/hooks/plymouth;
then
  exit 0
fi
sed -i -e 's|fc-cache -s|LD_PRELOAD=/usr/lib/unrandomize_uuid_generate_random.so fc-cache|' /usr/share/initramfs-tools/hooks/plymouth
EOF


# First build
sudo lb build | tee -a $LB_OUTPUT
RESULT=$?
if [ "$RESULT" != "0" ] ; then
   output_echo "Warning: lb config failed with $RESULT"
fi

# Move the image away
mkdir -p ${RESULTSDIR}/b1/${PROJECTNAME}/${CONFIGURATION}
mv live-image-amd64.hybrid.iso ${RESULTSDIR}/b1/${PROJECTNAME}/${CONFIGURATION}

# Clean for the second build
sudo lb clean --purge | tee -a $LB_OUTPUT
RESULT=$?
if [ "$RESULT" != "0" ] ; then
   output_echo "Warning: lb config failed with $RESULT"
fi

# Re-activate the previous configuration
lb config

# Second build
sudo lb build | tee -a $LB_OUTPUT
RESULT=$?
if [ "$RESULT" != "0" ] ; then
   output_echo "Warning: lb config failed with $RESULT"
fi

# Move the image away
mkdir -p ${RESULTSDIR}/b2/${PROJECTNAME}/${CONFIGURATION}
mv live-image-amd64.hybrid.iso ${RESULTSDIR}/b2/${PROJECTNAME}/${CONFIGURATION}

# Clean up
sudo lb clean --purge | tee -a $LB_OUTPUT
RESULT=$?
if [ "$RESULT" != "0" ] ; then
   output_echo "Warning: lb config failed with $RESULT"
fi

# The workspace is no longer required
cd ..

# Compare the images
TIMEOUT="240m"
DIFFOSCOPE="$(schroot --directory /tmp -c chroot:jenkins-reproducible-${DBDSUITE}-diffoscope diffoscope -- --version 2>&1)"
TMPDIR=${RESULTSDIR}
call_diffoscope ${PROJECTNAME} ${CONFIGURATION}/live-image-amd64.hybrid.iso
# List the content of the results directory
PAGE=${CONFIGURATION}.html
if [ -f "${RESULTSDIR}/${PROJECTNAME}/${CONFIGURATION}/live-image-amd64.hybrid.iso.html" ] ; then
   # Publish the output of diffoscope, there are differences
   cp -a ${RESULTSDIR}/${PROJECTNAME}/${CONFIGURATION}/live-image-amd64.hybrid.iso.html ${PAGE}
   save_artifacts debian_live_build ${CONFIGURATION} https://wiki.debian.org/ReproducibleInstalls/LiveImages
else
   if [ "$RESULT" != "0" ] ; then
      echo "${DIFFOSCOPE} for ${PROJECTNAME} in configuration ${CONFIGURATION} returned error code ${RESULT}" > ${PAGE}
      save_artifacts debian_live_build ${CONFIGURATION} https://wiki.debian.org/ReproducibleInstalls/LiveImages
   else
      echo "${PROJECTNAME} in configuration ${CONFIGURATION} is reproducible" > ${PAGE}
   fi
fi
publish_page debian_live_build

cleanup success
# Turn off the trap
trap - INT TERM EXIT

output_echo "the end, diffoscope has returned ${RESULT}."
# Return with the status of diffoscope
exit ${RESULT}
